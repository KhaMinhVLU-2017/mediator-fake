using System.Threading.Tasks;

namespace mediatorSelf
{
    public abstract class RequestHandler<Req, Res> where Req : Request<Res>
                                                      where Res : class
    {
        public abstract Task<Res> HandlerAsync(Req request);
    }
}