﻿using System;
using mediatorSelf.Models;
using System.Threading.Tasks;

namespace mediatorSelf
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var mediator = new MediatorMin();
            var query = new QueryCar() { Id = 1 };
            var queryPlane = new QueryAirPlane { search = "Find boeing" };

            var response = await mediator.Send(query);
            var responsePlan = await mediator.Send(queryPlane);
            Console.WriteLine(response.Name);
            Console.WriteLine(responsePlan.Name);
        }
    }
}
