using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace mediatorSelf
{
    public class MediatorMin
    {

        public Task<object?> Send(object req)
        {
            var command = Activator.CreateInstance(req.GetType());
            var typeRequest = command.GetType();
            var typeBase = typeRequest.BaseType;
            Type typeResponseDto = typeBase.GetGenericArguments().First();

            // Declare request handle
            Type typeRequestHandler = typeof(RequestHandler<,>);
            // Get list assembly with type RequestHandler
            var assembly = Assembly.GetAssembly(typeRequestHandler).GetTypes();

            // Find type requesthandler fit with req
            foreach (var item in assembly)
            {
                if (item.BaseType == null)
                    continue;

                if (!item.BaseType.GetGenericArguments().Any())
                    continue;

                var lstAgr = item.BaseType.GetGenericArguments();
                var isCheck = lstAgr.Where(s => s == typeRequest || s == typeResponseDto).Count() == 2;
                if (isCheck)
                {
                    typeRequestHandler = item;
                    isCheck = false;
                }
            }

            // Create object
            var requestHandler = Activator.CreateInstance(typeRequestHandler);
            MethodInfo handlerType = requestHandler.GetType().GetMethod("HandlerAsync");
            var response = handlerType.Invoke(requestHandler, new[] { req });
            var getResult = response.GetType().GetProperty("Result").GetValue(response);

            Console.WriteLine(getResult.GetType() == typeResponseDto);

            var result = Convert.ChangeType(getResult, typeResponseDto);

            return Task.FromResult(result);
        }

        public Task<TResponse> Send<TResponse>(IRequest<TResponse> req)
        {
            var typeRequest = req.GetType();
            var typeBase = typeRequest.BaseType;
            Type typeResponseDto = typeBase.GetGenericArguments().First();

            // Declare request handle
            Type typeRequestHandler = typeof(RequestHandler<,>);
            // Get list assembly with type RequestHandler
            var assembly = Assembly.GetAssembly(typeRequestHandler).GetTypes();

            // Find type requesthandler fit with req
            foreach (var item in assembly)
            {
                if (item.BaseType == null)
                    continue;

                if (!item.BaseType.GetGenericArguments().Any())
                    continue;

                var lstAgr = item.BaseType.GetGenericArguments();
                var isCheck = lstAgr.Where(s => s == typeRequest || s == typeResponseDto).Count() == 2;
                if (isCheck)
                {
                    typeRequestHandler = item;
                    isCheck = false;
                }
            }

            // Create object
            var requestHandler = Activator.CreateInstance(typeRequestHandler);
            MethodInfo handlerType = requestHandler.GetType().GetMethod("HandlerAsync");
            var response = handlerType.Invoke(requestHandler, new[] { req });

            return (Task<TResponse>)response;
        }
    }
}