namespace mediatorSelf.Models
{
    public class AirPlaneDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}