using System.Threading.Tasks;

namespace mediatorSelf.Models
{
    public class QueryCarHandler : RequestHandler<QueryCar, CarDto>
    {
        public override Task<CarDto> HandlerAsync(QueryCar request)
        {
            return Task.FromResult(new CarDto { Name = "Porsche" });
        }
    }
}