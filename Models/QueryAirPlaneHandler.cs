using System.Threading.Tasks;

namespace mediatorSelf.Models
{
    public class QueryAirPlaneHandler : RequestHandler<QueryAirPlane, AirPlaneDto>
    {
        public override Task<AirPlaneDto> HandlerAsync(QueryAirPlane request)
        {
            string search = request.search;
            var airPlane = new AirPlaneDto
            {
                Id = 9028,
                Name = "Boeing 747"
            };
            return Task.FromResult(airPlane);
        }
    }
}